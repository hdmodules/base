<?php

namespace hdmodules\base\cropbox;

use yii\web\AssetBundle;


class WidgetAsset extends AssetBundle
{
    public $sourcePath = '@vendor/hdmodules/base/cropbox/assets';
    public $css = [
        'style.css',
    ];
    public $depends = [
        'hdmodules\base\cropbox\CropboxAsset',
    ];
}

