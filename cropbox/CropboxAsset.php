<?php

namespace hdmodules\base\cropbox;

use yii\web\AssetBundle;


class CropboxAsset extends AssetBundle
{
    public $depends = [
        'hdmodules\base\cropbox\MouseWheelAsset',
    ];
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = '@vendor/hdmodules/base/cropbox/assets';
        $this->css = ['jquery.cropbox.css'];
        $this->js = ['jquery.cropbox.js'];
        parent::init();
    }
}
