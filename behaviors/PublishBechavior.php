<?php
namespace hdmodules\base\behaviors;

use Yii;
use yii\db\ActiveRecord;
use hdmodules\base\models\Publish;

class PublishBechavior extends \yii\base\Behavior
{
    private $_publish = null;
    private $_publish_set = [];

    public $table_name;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }

    public function afterInsert()
    {
        foreach ($this->publishSet as $key=>$publish){
            if($publish->loadPublishSet(Yii::$app->request->post(), $key)) {
                $publish->save();
            }
        }
    }

    public function afterUpdate()
    {
        foreach ($this->publishSet as $key=>$publish){
            if($publish->loadPublishSet(Yii::$app->request->post(), $key)){
                if($publish->isNewRecord){
                    $model = new Publish([
                        'model_id' => $this->owner->primaryKey,
                        'table_name' => $this->table_name,
                        'lang' => $publish->lang,
                        'status' => $publish->status,
                    ]);
                    
                    $model->save();
                } else {
                    $publish->update();
                }
            }
        }
    }

    public function afterDelete()
    {
        foreach ($this->publishSet as $publish){
            if(!$publish->isNewRecord){
                $publish->delete();
            }
        }
    }

// Publish -------------------------------------------------------------------------------------------------------------

    public function getRelPublish()
    {
        return $this->owner->hasOne(Publish::className(), ['model_id' => $this->owner->primaryKey()[0]])->where(['table_name' => $this->table_name, 'lang' => Yii::$app->language]);
    }

    public function getPublish()
    {
        if(!$this->_publish)
        {
            $this->_publish = $this->owner->relPublish;
            if(!$this->_publish)
            {
                $this->_publish = new Publish([
                    'model_id' => $this->owner->primaryKey,
                    'table_name' => $this->table_name,
                    'lang' => Yii::$app->language,
                ]);
            }
        }
        return $this->_publish;
    }

// PublishSet ----------------------------------------------------------------------------------------------------------

    public function getRelPublishSet()
    {
        return $this->owner->hasMany(Publish::className(), ['model_id' => $this->owner->primaryKey()[0]])->where(['table_name' => $this->table_name]);
    }

    public function getPublishSet()
    {
        if(empty($this->_publish_set)) {

            $this->_publish_set = $this->owner->relPublishSet;

            if(empty($this->_publish_set)){

                if(isset(Yii::$app->params['mlConfig']['languages']) && !empty(Yii::$app->params['mlConfig']['languages'])){
                    foreach (Yii::$app->params['mlConfig']['languages'] as $lang_key=>$lang_slug){
                        $this->_publish_set[] = new Publish([
                            'model_id' => $this->owner->primaryKey,
                            'table_name' => $this->table_name,
                            'lang' => $lang_key,
                        ]);
                    }
                }
            }
        }

        return $this->_publish_set;
    }

}