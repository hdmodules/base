<?php
namespace hdmodules\base\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * Sortable behavior. Enables model to be sorted manually by admin
 * @package hdmodules\base\behaviors
 */

class SortableModel extends Behavior
{
    public $par_id  = 'parent_id';

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'findMaxOrderNum',
        ];
    }

    public function findMaxOrderNum()
    {
        if(isset($this->owner->{$this->par_id})) {
            $maxOrderNum = (int)(new Query())
                ->select('MAX(`order_num`)')
                ->from($this->owner->tableName())
                ->where([$this->par_id  => $this->owner->{$this->par_id}])
                ->scalar();
            $this->owner->order_num = ++$maxOrderNum;
            
        } elseif(!$this->owner->order_num) {
            $maxOrderNum = (int)(new Query())
                ->select('MAX(`order_num`)')
                ->from($this->owner->tableName())
                ->scalar();
            $this->owner->order_num = ++$maxOrderNum;
        }
    }
}