<?php
namespace hdmodules\base\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use hdmodules\base\models\Tag;
use hdmodules\base\models\TagRelation;
use yii\helpers\ArrayHelper;

class Taggable extends Behavior
{
    private $_tags;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    public function getTagRelations()
    {
        return $this->owner->hasMany(TagRelation::className(), ['model_id' => $this->owner->primaryKey()[0]])->where(['table_name' => $this->owner->getTableSchema()->name]);
    }

    public function getTags()
    {
        return $this->owner->hasMany(Tag::className(), ['id' => 'tag_id'])->via('tagRelations');
    }

    public function getTagNames()
    {
        return implode(', ', $this->getTagsArray());
    }

    public function setTagNames($values)
    {
        $this->_tags = $this->filterTagValues($values);
    }

    public function getTagsArray()
    {
        if($this->_tags === null){
            $this->_tags = [];
            foreach($this->owner->tags as $tag) {
                $this->_tags[] = $tag->name;
            }
        }
        return $this->_tags;
    }

    public function afterSave()
    {
        $TagRelations = [];
        $exist_tags = ArrayHelper::map($this->owner->tags, 'id', 'name');

        if(count($this->_tags)) {

            foreach ($this->_tags as $name) {

                if(!in_array($name, $exist_tags)){
                    if (!($tag = Tag::findOne(['name' => $name]))) {
                        $tag = new Tag(['name' => $name]);
                    }
                    $tag->frequency++;
                    if ($tag->save()) {
                        $TagRelations[] = [$this->owner->getTableSchema()->name, $this->owner->primaryKey, $tag->id];
                    }
                }else{
                    $key = array_search($name, $exist_tags);
                    unset($exist_tags[$key]);
                }
            }

            if(count($TagRelations)) {
                Yii::$app->db->createCommand()->batchInsert(TagRelation::tableName(), ['table_name', 'model_id', 'tag_id'], $TagRelations)->execute();
            }


        }

        if(count($exist_tags)){
            foreach ($exist_tags as $k=>$val){
                TagRelation::deleteAll(['table_name' => $this->owner->getTableSchema()->name, 'model_id' => $this->owner->primaryKey, 'tag_id'=>$k]);
                $tag = Tag::findOne(['id' => $k]);
                $tag->frequency = ($tag->frequency)-1;
                $tag->update();
            }
            Tag::deleteAll(['frequency' => 0]);
        }
    }

    public function beforeDelete()
    {
        $pks = [];

        foreach($this->owner->tags as $tag){
            $pks[] = $tag->primaryKey;
        }

        if (count($pks)) {
            Tag::updateAllCounters(['frequency' => -1], ['in', 'id', $pks]);
        }
        Tag::deleteAll(['frequency' => 0]);
        TagRelation::deleteAll(['table_name' => $this->owner->getTableSchema()->name, 'model_id' => $this->owner->primaryKey]);
    }

    /**
     * Filters tags.
     * @param string|string[] $values
     * @return string[]
     */
    public function filterTagValues($values)
    {
        return array_unique(preg_split(
            '/\s*,\s*/u',
            preg_replace('/\s+/u', ' ', is_array($values) ? implode(',', $values) : $values),
            -1,
            PREG_SPLIT_NO_EMPTY
        ));
    }
}