<?php
namespace hdmodules\base\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;
use hdmodules\base\models\Menu;
use hdmodules\base\models\MenuItem;
use hdmodules\base\controllers\Controller;
use hdmodules\base\controllers\StatusController;

class MenuController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'menu';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'status' => [
                'class' => StatusController::className(),
                'model' => MenuItem::className(),
            ],
        ];
    }

    /**
     * List all menus
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Menu::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    /**
     * View menu
     * @return string
     */
    public function actionView($id)
    {
        $model = Menu::find()->where(['id' => $id])->one();
        //$menuItem = new MenuItem();
        $menuItem = MenuItem::find()->where(['menu_id' => $model->id, 'parent_id' => null])->orderBy(["order_num" => SORT_ASC])->all();
        return $this->render('view', [
            'items' => $menuItem,
            'model' => $model
        ]);
    }
    
    public function getTree($model_id, $items, $prefix = ''){
        $html = '';
        if($items){
            $i = 0;
            $prefix = !empty($prefix) ? $prefix . '.' : '';
            foreach ($items as $item){
                $i++;
                $children = $item->getChildren()->all();
                $counter = $prefix . $i;
                $html .= $this->renderPartial('/menu-item/_items', ['item' => $item, 'model_id' => $model_id, 'children' => $children, 'prefix' => $counter]);
            }
        }
        return $html;
    }
    /**
     * Create a menu
     * @return array|string|Response
     */
    public function actionCreate()
    {
        $model = new Menu();

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    $this->flash('success', Yii::t('base', 'Menu created'));
                    return $this->redirect(['/base/menu/index']);
                } else {
                    $this->flash('error', Yii::t('base', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Edit menu
     * @param $id
     * @return array|string|Response
     */
    public function actionUpdate($id)
    {
        $model = Menu::findOne($id);

        if($model === null){
            $this->flash('error', Yii::t('base', 'Not found'));
            return $this->redirect(['index']);
        }

        if(isset($_POST['Menu'])) {
            if ($model->load(Yii::$app->request->post())) {
                if(Yii::$app->request->isAjax){
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
                else{
                    if($model->save()){
                        $this->flash('success', Yii::t('base', 'Menu updated'));
                        return $this->redirect(['/base/menu/index']);
                    }
                    else{
                        $this->flash('error', Yii::t('base', 'Update error. {0}', $model->formatErrors()));
                    }
                    return $this->refresh();
                }
            }
        } else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

    public function actionDelete($id)
    {
        if (($model = Menu::findOne($id))) {
            $model->delete();
        }
        
        return $this->redirect(['index']);
    }

}