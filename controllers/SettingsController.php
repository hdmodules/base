<?php
namespace hdmodules\base\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Response;
use yii\widgets\ActiveForm;
use hdmodules\base\models\Setting;
use hdmodules\base\controllers\Controller;

class SettingsController extends Controller
{

    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => Setting::find()->where(['>=', 'visibility', true ? Setting::VISIBLE_ROOT : Setting::VISIBLE_ALL]),
        ]);

        return $this->render('index', [
            'data' => $data
        ]);
    }

    public function actionCreate()
    {
        $model = new Setting;

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if($model->save()){
                    $this->flash('success', Yii::t('base', 'Setting created'));
                    return $this->redirect('index');
                }
                else{
                    $this->flash('error', Yii::t('base', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        }
        else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    public function actionEdit($id)
    {
        $model = Setting::findOne($id);

        if($model === null || ($model->visibility < (true ? Setting::VISIBLE_ROOT : Setting::VISIBLE_ALL))){
            $this->flash('error', Yii::t('base', 'Not found'));
            return $this->redirect(['index']);
        }

        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else{
                if($model->save()){
                    $this->flash('success', Yii::t('base', 'Setting updated'));
                }
                else{
                    $this->flash('error', Yii::t('base', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model
            ]);
        }
    }

    public function actionDelete($id)
    {
        if(($model = Setting::findOne($id))){
            $model->delete();
        } else {
            $this->error = Yii::t('base', 'Not found');
        }
        return $this->formatResponse(Yii::t('base', 'Setting deleted'));
    }
}