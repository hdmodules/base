<?php
namespace hdmodules\base\controllers;

use Yii;
use yii\base\Behavior;
use hdmodules\base\components\ActiveRecord;

class StatusController extends Behavior
{
    /**
     * @var ActiveRecord
     */
    public $model;

    /**
     * @param $id
     * @param $status
     * @return mixed
     */
    public function changeStatus($id, $status)
    {
        $modelClass = $this->model;

        if(($model = $modelClass::findOne($id))){
            $model->status = $status;
            $model->update();
        }
        else{
            $this->error = Yii::t('base', 'Not found');
        }

        return $this->owner->formatResponse(Yii::t('base', 'Status successfully changed'));
    }

    public function changeStatusNoIndex($id, $status)
    {
        $modelClass = $this->model;

        if(($model = $modelClass::findOne($id))){
            $model->no_index = $status;
            $model->update();
        }
        else{
            $this->error = Yii::t('base', 'Not found');
        }

        return $this->owner->formatResponse(Yii::t('base', 'Status successfully changed'));
    }
}