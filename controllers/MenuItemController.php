<?php
namespace hdmodules\base\controllers;

use Yii;
use hdmodules\base\controllers\Controller;
use hdmodules\base\models\MenuItem;
use hdmodules\base\controllers\SortableController;
use hdmodules\base\controllers\StatusController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class MenuItemController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => SortableController::className(),
                'model' => MenuItem::className(),
            ],
            [
                'class' => StatusController::className(),
                'model' => MenuItem::className()
            ]
        ];
    }

    /**
     * Create a menu item
     * @return array|string|Response
     */
    public function actionCreate($menu_id, $parent_id = null)
    {
        
        $model = new MenuItem();
        $menu = \hdmodules\base\models\Menu::find()->where(['id' => $menu_id])->one();
        
        $parent = null;
        if(isset($parent_id)){
            $parent = MenuItem::find()->where(['id' => $parent_id])->one();
        }
        $model->parent_id = $parent_id;
        //Redirect back if menu not exist
        if(!$menu) {
            return $this->redirect(['/base/menu/update', 'id' => $menu->menu_id]);
        }

        $model->menu_id = $menu->id;
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {

                if ($model->save()) {
                    $this->flash('success', Yii::t('base', 'Menu item created'));
                    return $this->redirect(['/base/menu/view', 'id' => $model->menu_id]);
                } else {
                    $this->flash('error', Yii::t('base', 'Create error. {0}', $model->formatErrors()));
                    return $this->refresh();
                }
            }
        } else {

            return $this->render('create', [
                'model' => $model,
                'menu' => $menu,
                'parent' => $parent
            ]);
        }

    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (($model = MenuItem::findOne($id))) {
            if(isset($model->children)&& !empty($model->children)){
                foreach($model->children as $child){
                    $child->delete();
                }
            }

            $model->delete();
        } else {
            $this->error = Yii::t('base', 'Not found');
        }
        return $this->formatResponse(Yii::t('base', 'Menu item deleted'));
    }

    /**
     * Edit menu
     * @param $id
     * @return array|string|Response
     */
    public function actionUpdate($id)
    {
        $model = MenuItem::findOne($id);
        $menu = \hdmodules\base\models\Menu::find()->where(['id' => $model->menu_id])->one();
        
        if($model === null){
            $this->flash('error', Yii::t('base', 'Not found'));
            return $this->redirect(['/base/menu/index']);
        }

        $model->loadTranslations_custom();
        
        if ($model->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isAjax){
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            else {

                if($model->save()){
                    $this->flash('success', Yii::t('base', 'Menu item updated'));
                }
                else{
                    $this->flash('error', Yii::t('base', 'Update error. {0}', $model->formatErrors()));
                }
                return $this->refresh();
            }
        }
        else {
            return $this->render('edit', [
                'model' => $model,
                'menu' => $menu
            ]);
        }
    }

        /**
     * @param $id
     * @return mixed
     */
    public function actionUp($id)
    {
        $menu_id = MenuItem::moveUp($id);

        return $this->redirect(['menu/view', 'id' => $menu_id]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionDown($id)
    {
        $menu_id = MenuItem::moveDown($id);
        
        return $this->redirect(['menu/view', 'id' => $menu_id]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionOn($id)
    {
        return $this->changeStatus($id, MenuItem::STATUS_ON);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionOff($id)
    {
        return $this->changeStatus($id, MenuItem::STATUS_OFF);
    }
}