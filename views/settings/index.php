<?php
use hdmodules\base\models\Setting;
use yii\helpers\Url;

$this->title = Yii::t('base', 'Settings');
?>

<?= $this->render('_menu') ?>

<?php if($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
        <tr>
            <th width="50">#</th>
            <th><?= Yii::t('base', 'Name') ?></th>
            <th><?= Yii::t('base', 'Title') ?></th>
            <th><?= Yii::t('base', 'Value') ?></th>
            <th width="30"></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($data->models as $setting) : ?>
            <tr <?php if($setting->visibility == Setting::VISIBLE_ROOT) echo 'class="warning"'?>>
                <td><?= $setting->primaryKey ?></td>
                <td><?= $setting->name ?></td>
                <td><a href="<?= Url::to(['/base/settings/edit', 'id' => $setting->primaryKey]) ?>" title="<?= Yii::t('base', 'Edit') ?>"><?= $setting->title ?></a></td>
                <td style="overflow: hidden"><?= $setting->value ?></td>
                <td><a href="<?= Url::to(['/base/settings/delete', 'id' => $setting->primaryKey]) ?>" class="glyphicon glyphicon-remove confirm-delete" title="<?= Yii::t('base', 'Delete item') ?>"></a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <?= yii\widgets\LinkPager::widget([
            'pagination' => $data->pagination
        ]) ?>
    </table>
<?php else : ?>
    <p><?= Yii::t('base', 'No records found') ?></p>
<?php endif; ?>