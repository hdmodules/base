<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use hdmodules\base\models\Setting;
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">

        <div class="x_title">
            <h2><?= Yii::t('content', 'Form') ?> <small><?= Yii::t('base', 'create setting') ?></small></h2>
            <ul class="nav navbar-right">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">

            <?php $form = ActiveForm::begin(['enableAjaxValidation' => true]); ?>

                <?= $form->field($model, 'name')->textInput(!$model->isNewRecord ? ['disabled' => 'disabled'] : []) ?>
                <?= $form->field($model, 'visibility')->checkbox(['uncheck' => Setting::VISIBLE_ALL]) ?>
                <?= $form->field($model, 'title')->textarea(['disabled' => false]) ?>
                <?= $form->field($model, 'value')->textarea() ?>

                <?= Html::submitButton(Yii::t('base', 'Save'), ['class' => 'btn btn-primary']) ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    </div>
</div>

