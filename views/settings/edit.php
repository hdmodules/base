<?php
$this->title = Yii::t('base', 'Edit setting');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>