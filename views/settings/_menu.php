<?php
use yii\helpers\Url;

$action = $this->context->action->id;
?>
<?php if(true) : ?>
    <ul class="nav nav-pills">
        <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
            <a href="<?= Url::to([('/base/settings/index')]) ?>">
                <?php if($action === 'edit') : ?>
                    <i class="glyphicon glyphicon-chevron-left font-12"></i>
                <?php endif; ?>
                <?= Yii::t('base', 'List settings') ?>
            </a>
        </li>
        <li <?= ($action === 'create') ? 'class="active"' : '' ?>><a href="<?= Url::to([('/base/settings/create')]) ?>"><?= Yii::t('base', 'Create') ?></a></li>
    </ul>
    <br/>
<?php elseif($action === 'edit') : ?>
    <ul class="nav nav-pills">
        <li>
            <a href="<?= Url::to([('/base/settings/index')]) ?>">
                <i class="glyphicon glyphicon-chevron-left font-12"></i>
                <?= Yii::t('base', 'Back') ?>
            </a>
        </li>
    </ul>
    <br/>
<?php endif; ?>