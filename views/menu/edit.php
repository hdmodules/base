<?php
/**
 * @var $model hdmodules\base\models\MenuItem
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = sprintf('Update: (ID: %s) %s', $model->id, $model->name);

$this->params['breadcrumbs'][] = ['label' => 'Menu', ['/base/menu/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><?= Html::encode($this->title) ?></h2>


                <ul class="nav navbar-right">
                    <li><a class="collapse-link" style="cursor:pointer"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="row">

                    <div class="clearfix"></div>
                </div>

                <?=
                    yii\widgets\Breadcrumbs::widget([
                        'homeLink' => [
                            'label' => 'Home',
                            'url' => Yii::$app->urlManager->baseUrl,
                        ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []
                    ]);
                ?>

            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-lg-12">
                        <?= $this->render('_form', ['model' => $model]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




