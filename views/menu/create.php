<?php

use yii\helpers\Html;

$this->title = 'Create menu';

$this->params['breadcrumbs'][] = ['label' => 'Menu', ['base/menu/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><?= Html::encode($this->title) ?></h2>


                <ul class="nav navbar-right">
                    <li><a class="collapse-link" style="cursor:pointer"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="row">

                    <div class="clearfix"></div>
                </div>

                <?= $this->render('_menu') ?> 

            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-lg-12">
                        <?= $this->render('_form', ['model' => $model]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>