<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = Yii::t('base', 'Menu');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php //$this->render('_menu') ?>



<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-md-10">
        <?=
        yii\widgets\Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Home',
                'url' => Yii::$app->urlManager->baseUrl,
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []])
        ?>
    </div>
    <div class="col-md-2" align="right">
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'contentOptions' => ['width' => '3%']
                ],
                [
                    'attribute' => 'id',
                    'contentOptions' => ['width' => '3%'],
                ],
                [
                    'attribute' => 'name',
                    'contentOptions' => ['width' => '40%'],
                ],
                [
                    'attribute' => 'slug',
                    'contentOptions' => ['width' => '10%'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['width' => '3%'],
                //'template' => '{update} {delete}'
                ],
            ],
        ]);
        ?>

    </div>
</div>
