<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use hdmodules\base\multilanguage\input_widget\MultiLanguageActiveField;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>

<?= $form->field($model, 'name')->widget(MultiLanguageActiveField::className()) ?>
<?= $form->field($model, 'slug') ?>

<div align="right">
    <?= Html::submitButton(Yii::t('base', 'Save'), ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
