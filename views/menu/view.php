<?php

use yii\widgets\Pjax;
use yii\web\JsExpression;
use execut\widget\TreeView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use hdmodules\base\multilanguage\input_widget\MultiLanguageActiveField;
use kartik\widgets\SwitchInput;
use yii\web\View;

$this->title = sprintf('(ID: %s) %s', $model->id, $model->name);

$this->params['breadcrumbs'][] = ['label' => 'Menu', ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
    function changeStatus(obj){
        console.log(obj)
        var item_id = $(obj).data("id");

        if(obj.checked){
            var url = "' . Url::to(['/base/menu-item/on']) . '";
        } else {
            var url = "' . Url::to(['/base/menu-item/off']) . '";
        }

        $.ajax({
            url: url,
            data: {id:item_id},
            method: "GET",
            dataType: "json",
            success: function(data){
                console.log(data );
            }
        });
        
    };', View::POS_END);
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-md-10">
        <?= $this->render('_menu') ?> 
    </div>
</div>
<div>
    <a class="btn btn-primary" href="<?= Url::to(['menu-item/create', 'menu_id' => $model->id]) ?>">
        <?= Yii::t("site", "Add menu item"); ?>
    </a>
    <br>
    <br>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $this->context->getTree($model->id, $items) ?>
    </div>
</div>