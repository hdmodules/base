<?php
use yii\helpers\Url;

$action = $this->context->action->id;

?>
<ul class="nav nav-pills">
    <li <?= ($action === 'index') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['menu/view', 'id' => $model->menu_id]) ?>">List</a>
    </li>
    
    <li <?= ($action === 'create') ? 'class="active"' : '' ?>><a href="<?= Url::to(['create']) ?>">Create</a></li>
    <?php if($action == 'update'){ ?>
        <li class="active"><a href="#">Update</a></li>
    <?php } ?>
</ul>
<br/>