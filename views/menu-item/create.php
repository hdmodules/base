<?php
/**
 * @var $menu  hdmodules\base\models\Menu
 * @var $model hdmodules\base\models\MenuItem
 */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = sprintf('Create item - (ID: %s) %s', $menu->id, $menu->name);
$this->params['breadcrumbs'][] = ['label' => 'Menu', '/base/menu/index'];
$this->params['breadcrumbs'][] = 'Create menu item';

?>

<div class="row">
    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="x_panel">

            <div class="x_title">
                <h2><?= Html::encode($this->title) ?></h2>


                <ul class="nav navbar-right">
                    <li><a class="collapse-link" style="cursor:pointer"><i class="fa fa-chevron-up"></i></a>
                    </li>
                </ul>
                <div class="row">

                    <div class="clearfix"></div>
                </div>

                <?= $this->render('_menu', ['model' => $model]) ?>  

            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-lg-12">
                        <?= $this->render('_form', ['model' => $model, 'menu' => $menu, 'parent' => $parent]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>