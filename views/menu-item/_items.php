<?php

use yii\helpers\Url;
use yii\helpers\Html;
use kartik\widgets\SwitchInput;

?>

<div class="row">
    <div class="col-md-8" style="padding-top: 10px;"><b><?= $prefix ?>. </b> <?= Html::a(sprintf('(ID: %s) %s', $item->id, $item->label), ['/base/menu-item/update', 'id' => $item->id]) ?></div>
    <div class="col-md-4"  align="right" style="padding-top: 10px;">


        <div class="btn-group btn-group-sm" role="group" style="margin-right: 30px;">
            <?=
            SwitchInput::widget([
                'name' => 'status',
                'options' => [
                    'class' => 'switch-input',
                    'data-id' => $item->id,
                   // 'data-link' => Url::to(['/base/menu-item/on']),
                   // 'onchange' => 'changeStatus("' . $item->id . '");'
                ],
                'value' => $item->status == \hdmodules\base\models\MenuItem::STATUS_ON,
                'pluginOptions' => [
                    'size' => 'small',
                    'onColor' => 'success',
                    'offColor' => 'danger',
                ],
                'pluginEvents' => [
                    "switchChange.bootstrapSwitch" => "function() { changeStatus(this); }",
                ]
            ]);
            ?>
        </div>
        <div class="btn-group btn-group-sm" role="group" style="margin-bottom: 10px;margin-right: 30px;">
            <?= Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['/base/menu-item/up', 'id' => $item->id], ['title' => Yii::t('base', 'Move up'), 'class' => 'btn btn-default move-up']) ?>
            <?= Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['/base/menu-item/down', 'id' => $item->id], ['title' => Yii::t('base', 'Move down'), 'class' => 'btn btn-default move-down']) ?>
        </div>
        <div class="btn-group btn-group-sm" role="group" style="margin-bottom: 10px;">
            <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', ['/base/menu-item/create', 'menu_id' => $model_id, 'parent_id' => $item->id], ['title' => 'Create sub menu', 'class' => 'btn btn-default']) ?>
            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['/base/menu-item/update', 'menu_id' => $model_id, 'id' => $item->id], ['title' => 'Create sub menu', 'class' => 'btn btn-default']) ?>
            <?= Html::a('<span class="glyphicon glyphicon-remove"></span>', ['/base/menu-item/delete', 'id' => $item->id], ['title' => Yii::t('base', 'Move down'), 'class' => 'btn btn-default confirm-delete']) ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12" style="padding-left:30px;">
        <?= $this->context->getTree($model_id, $children, $prefix) ?>
    </div>
</div>