<?php
/**
 * @var $model MenuItem
 */
use hdmodules\base\multilanguage\input_widget\MultiLanguageActiveField;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\View;

//$parentName = isset($parent) ? $parent->label : $menu->name . '(Root)';
$parentName = $model->menu->name;

//Add new param
$this->registerJs('
    function addParam(){
        var paramName = $("#paramName").val();
        var paramValue = $("#paramValue").val();
        if(paramName && paramValue){
            var paramsJson = $("#paramsJson").val();
            
            if(!paramsJson){
                paramsJson = "{}";
            }
            
            var obj = JSON.parse(paramsJson);

            obj[paramName] = paramValue;

            var jsonString = JSON.stringify(obj);
            $("#paramsJson").val(jsonString);
            
            showParamsList();
            
            $("#paramName").val("");
            $("#paramValue").val("");
        }

    }', View::POS_END);
//Show params list
$this->registerJs('
    function showParamsList(){
        var paramsJson = $("#paramsJson").val();
        
        if(paramsJson){
            var jsonObj = JSON.parse(paramsJson);
            var html = "";
            $.each(jsonObj, function(key, value){
                console.log(key, value);
                html +=  "<a onclick=\"deleteParam(\'" + key + "\');\" style=\"margin-right: 20px;cursor:pointer;\"><span class=\"glyphicon glyphicon-remove\"></span></a> <b>" + key + " => " + value + "</b><br>";
            });
            $("#params-list").html(html);
        }

    }', View::POS_END);
//Delete param 
$this->registerJs('
    function deleteParam(key){
        var paramsJson = $("#paramsJson").val();
        
        if(paramsJson){
            var jsonObj = JSON.parse(paramsJson);
            
            delete jsonObj[key];
            
            var jsonString = JSON.stringify(jsonObj);
            $("#paramsJson").val(jsonString);
            
            showParamsList();
        }

    }', View::POS_END);

$this->registerJs('showParamsList();', View::POS_READY);
?>

<?php $form = ActiveForm::begin(['id' => 'menu-item-form']); ?>
<div class="row>">
    <div class="col-md-12">
      <h3>Parent: <?= $parentName ?></h3>  
    </div>
</div>
<div class="row>">
    <div class="col-md-12">
      <?= $form->field($model, 'label')->widget(MultiLanguageActiveField::className()) ?>
    </div>
</div>
<div class="row>">
    <div class="col-md-12">
      <?= $form->field($model, 'route')->textInput() ?>
    </div>   
</div> 
<div class="row>">
    <div class="col-md-12">
      <label class="control-label">Params</label>
    </div>   
</div> 
<div class="row>">
    <div class="col-md-5">
      <?= Html::textInput('param', '', ['id' => 'paramName', 'class' => 'form-control', 'placeholder' => 'Name']) ?>
    </div>
    <div class="col-md-5">
      <?= Html::textInput('param_value', '', ['id' => 'paramValue', 'class' => 'form-control', 'placeholder' => 'Value']) ?>
    </div>
    <div class="col-md-2" align="left">
      <?= Html::button('Add', ['class' => 'btn btn-info', 'onclick' => 'addParam();return false;']) ?>
    </div>
</div>
<div class="row>">
    <div class="col-md-12" id="params-list"></div>   
</div> 
<div class="row>">
    <div class="col-md-12">
      <?= $form->field($model, 'params')->hiddenInput(['id' => 'paramsJson'])->label(false) ?>
    </div>
</div>    

    
    
    
    

<div align="right">
    <?= Html::submitButton(Yii::t('base', 'Save'), ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

<script>

</script>