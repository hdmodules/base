<?php
namespace hdmodules\base\validators;

use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\validators\Validator;

use hdmodules\base\models\Setting;


class ReCaptchaValidator extends Validator
{
    const SITE_VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';
    const CAPTCHA_RESPONSE_FIELD = 'g-recaptcha-response';

    public $skipOnEmpty = false;

    public static $widgetId = 1;

    public function init()
    {
        parent::init();


        if ($this->message === null) {
            $this->message = Yii::t('yii', 'The verification code is incorrect.');
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = Yii::t(
            'yii',
            '{attribute} cannot be blank.',
            ['attribute' => $model->getAttributeLabel($attribute)]
        );

        if(isset(self::$widgetId) && !empty(self::$widgetId)){
            return "(function(messages){if(!grecaptcha.getResponse(recaptcha".(self::$widgetId++).")){messages.push('{$message}');}})(messages);";
        }else{
            return "(function(messages){if(!grecaptcha.getResponse()){messages.push('{$message}');}})(messages);";
        }

    }

    protected function validateValue($value)
    {
        if (!Setting::get('recaptcha_secret')) {
            throw new InvalidConfigException('Required `recatpcha_secret` setting isn\'t set.');
        }

        if (empty($value)) {
            if (!($value = Yii::$app->request->post(self::CAPTCHA_RESPONSE_FIELD))) {
                return [$this->message, []];
            }
        }

        $request = self::SITE_VERIFY_URL . '?' . http_build_query(
                [
                    'secret' => Setting::get('recaptcha_secret'),
                    'response' => $value,
                    'remoteip' => Yii::$app->request->userIP
                ]
            );
        $response = $this->getResponse($request);
        if (!isset($response['success'])) {
            throw new Exception('Invalid recaptcha verify response.');
        }
        return $response['success'] ? null : [$this->message, []];
    }

    protected function getResponse($request)
    {
        $response = file_get_contents($request);
        return Json::decode($response, true);
    }
}