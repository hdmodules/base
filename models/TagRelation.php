<?php
namespace hdmodules\base\models;

use hdmodules\base\components\ActiveRecord;

class TagRelation extends ActiveRecord
{
    public static function tableName()
    {
        return 'tag_rel';
    }

    public function getTag(){
        return $this->hasOne(Tag::className(), ['tag_id' => 'id']);
    }
}