<?php
namespace hdmodules\base\models;

use Yii;

use yii\db\Exception;
use hdmodules\base\components\ActiveRecord;
use hdmodules\base\helpers\Data;
use hdmodules\base\behaviors\CacheFlush;

class Setting extends ActiveRecord
{
    const VISIBLE_NONE = 0;
    const VISIBLE_ROOT = 1;
    const VISIBLE_ALL = 2;

    const CACHE_KEY = 'settings';

    static $_data;

    public static function tableName()
    {
        return 'settings';
    }

    public function rules()
    {
        return [
            [['name', 'title', 'value'], 'required'],
            [['name', 'title', 'value'], 'trim'],
            ['name',  'match', 'pattern' => '/^[a-zA-Z][\w_-]*$/'],
            ['name', 'unique'],
            ['visibility', 'number', 'integerOnly' => true]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('base', 'Name'),
            'title' => Yii::t('base', 'Title'),
            'value' => Yii::t('base', 'Value'),
            'visibility' => Yii::t('base', 'Only for developer')
        ];
    }

    public function behaviors()
    {
        return [
            CacheFlush::className()
        ];
    }

    public static function get($name)
    {
        if(!self::$_data){
            self::$_data =  Data::cache(self::CACHE_KEY, 3600, function(){
                $result = [];
                try {
                    foreach (parent::find()->all() as $setting) {
                        $result[$setting->name] = $setting->value;
                    }
                }catch(Exception $e){}
                return $result;
            });
        }

        if(($name == 'admin_email' || $name == 'webinar_email') && isset(self::$_data[$name]) && is_string(self::$_data[$name])){
            $address = self::$_data[$name];
            self::$_data[$name] = explode(', ', $address);
        }

        return isset(self::$_data[$name]) ? self::$_data[$name] : null;
    }

    public static function set($name, $value)
    {
        if(self::get($name)){
            $setting = Setting::find()->where(['name' => $name])->one();
            $setting->value = $value;
        } else {
            $setting = new Setting([
                'name' => $name,
                'value' => $value,
                'title' => $name,
                'visibility' => self::VISIBLE_NONE
            ]);
        }
        $setting->save();
    }
}