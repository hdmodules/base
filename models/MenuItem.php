<?php

namespace hdmodules\base\models;

use Yii;
use yii\db\Query;
use yii\validators\BooleanValidator;
use yii\validators\NumberValidator;
use yii\validators\RequiredValidator;
use yii\validators\StringValidator;
use yii\behaviors\TimestampBehavior;
use hdmodules\base\behaviors\SortableModel;
use hdmodules\base\components\ActiveRecord;
use hdmodules\base\multilanguage\models\TranslationsWithString;
use hdmodules\base\multilanguage\MultiLanguageBehavior;
use hdmodules\base\multilanguage\MultiLanguageTrait;

/**
 * @property int $menu_id
 * @property string $label
 * @property string $path
 */
class MenuItem extends ActiveRecord {

    use MultiLanguageTrait;

    const STATUS_ON = 1;
    const STATUS_OFF = 0;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return "menu_item";
    }

    /**
     * @return array
     */
    public function rules() {
        return [
            [['menu_id', 'label', 'route'], 'required'],
            ['label', StringValidator::className()],
            ['label', RequiredValidator::className()],
            ['status', BooleanValidator::className()],
            ['order_num', NumberValidator::className()],
            [['route', 'params', 'parent_id'], 'safe'],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function behaviors() {
        return [
            'sortable' => SortableModel::className(),
            'mlBehavior' => [
                'class' => MultiLanguageBehavior::className(),
                'mlConfig' => [
                    'db_table' => 'translations_with_string',
                    'attributes' => ['label', 'path'],
                    'admin_routes' => [
                        'admin/*',
                    ]
                ],
            ],
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            'label' => Yii::t("site", "Label"),
            'path' => Yii::t("site", "Url"),
            'parent_id' => Yii::t("site", "Parent Menu Item"),
            'model_id' => Yii::t("site", "Page relation"),
        ];
    }

    /**
     * Get associated menu
     * @return \yii\db\ActiveQuery
     */
    public function getMenu() {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * Get direct children
     * @return \yii\db\ActiveQuery
     */
   /* public function getChildren() {
        return $this->hasMany(MenuItem::className(), ['parent_id' => 'menu_item_id'])->with(['translations'])->orderBy(["order_num" => SORT_ASC]);
    }*/

    public function getTranslations() {
        return $this->hasMany(TranslationsWithString::className(), ['model_id' => 'menu_id'])->andWhere(['table_name' => 'site', 'lang' => Yii::$app->language]);
    }
    
    /**
     * Get direct children
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(MenuItem::className(), ['parent_id' => 'id'])->with(['translations'])->orderBy(["order_num" => SORT_ASC]);
    }
    /**
     * Get associated menu
     * @return \yii\db\ActiveQuery
     */
    public function getPage() {
        if ($this->model_id && $this->table_name) {

            if ($this->table_name == 'page') {
                return $this->hasOne(Page::className(), ['page_id' => 'model_id']);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Recalculates order of elements in a node
     * @param int $parent_id
     * @param $menu_id
     */
    public static function fixOrder($parent_id, $menu_id) {
        if ($parent_id) {
            $row = self::find()->where(['menu_id' => $menu_id, 'parent_id' => $parent_id])->orderBy(["order_num" => SORT_ASC])->all();
        } else {
            $row = self::find()->where(['menu_id' => $menu_id, 'parent_id' => 0])->orderBy(["order_num" => SORT_ASC])->all();
        }

        $i = 0;
        $str = "Fixing order: \n";
        foreach ($row as $item) {
            $str .= "(" . $item->id . "): " . $item->order_num . " -> " . $i . ", \n";
            $item->order_num = $i;
            $i++;
            $item->save(false);
        }

        Yii::trace($str);
    }

    /**
     * Moves an item down one level
     * @param int $pk item primary key
     */
    public static function moveDown($pk) {
        /** @var MenuItem $curr */
        $curr = self::findOne($pk);

        self::fixOrder($curr->parent_id, $curr->menu_id);

        /** @var MenuItem $next */
        $next = self::find()->where([
                    'menu_id' => $curr->menu_id,
                    'parent_id' => $curr->parent_id,
                    'order_num' => ($curr->order_num + 1)
                ])->one();

        Yii::trace("next: " . $next->primaryKey . "curr: " . $curr->primaryKey);

        if ($curr && $next) {
            $next->order_num = ($next->order_num - 1);
            $curr->order_num = ($curr->order_num + 1);
            $next->save(false);
            $curr->save(false);
        }
        
        return $curr->menu_id;
    }

    /**
     * Moves an item up one level
     * @param int $pk item primary key
     */
    public static function moveUp($pk) {
        /** @var MenuItem $curr */
        $curr = self::findOne($pk);

        self::fixOrder($curr->parent_id, $curr->menu_id);

        /** @var MenuItem $next */
        $next = self::find()->where([
                    'menu_id' => $curr->menu_id,
                    'parent_id' => $curr->parent_id,
                    'order_num' => ($curr->order_num - 1)
                ])->one();


        if ($curr && $next) {
            $next->order_num = $next->order_num + 1;
            $curr->order_num = $curr->order_num - 1;
            $next->save(false);
            $curr->save(false);
        }
        
        return $curr->menu_id;
    }

    public function createUrl() {

        if (!empty($this->path)) {
            return $this->path;
        } else {
            $page = $this->page;
            if ($page && !empty($page->url)) {
                return '/' . $page->url;
            } else {
                return $this->path;
            }
        }
    }

}
