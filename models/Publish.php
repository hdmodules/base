<?php
namespace hdmodules\base\models;

use hdmodules\base\components\ActiveRecord;

class Publish extends ActiveRecord
{
    const UNPUBLISHED = 0;
    const PUBLISHED = 1;

    public static function tableName()
    {
        return 'publish';
    }

    public function rules()
    {
        return [
            [['model_id', 'status'], 'integer'],
            [['model_id', 'table_name', 'lang', 'status'], 'safe'],
            ['status', 'default', 'value' => 0],

        ];
    }

    public function loadPublishSet($data, $key)
    {
        $scope =  $this->formName();
        if (isset($data[$scope])) {
            $this->setAttributes($data[$scope][$key]);

            return true;
        } else {
            return false;
        }
    }
}