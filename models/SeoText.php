<?php
namespace hdmodules\base\models;

use Yii;

use hdmodules\base\multilanguage\MultiLanguageBehavior;
use hdmodules\base\multilanguage\MultiLanguageTrait;

use hdmodules\base\components\ActiveRecord;
use hdmodules\base\validators\EscapeValidator;

class SeoText extends ActiveRecord
{
    use MultiLanguageTrait;

    public function behaviors()
    {
        return [
            'mlBehavior' => [
                'class' => MultiLanguageBehavior::className(),
                'mlConfig' => [
                    'db_table' => 'translations_with_string',
                    'attributes' => ['h1', 'title', 'keywords', 'description'],
                    'admin_routes' => [
                        'admin/*'
                    ],
                ],
            ],
        ];
    }

    public static function tableName()
    {
        return 'seotext';
    }

    public function rules()
    {
        return [
            [['h1', 'title', 'keywords', 'description'], 'trim'],
            [['h1', 'title', 'keywords', 'description'], 'string', 'max' => 255],
            [['h1', 'title', 'keywords', 'description'], EscapeValidator::className()],
        ];
    }

    public function attributeLabels()
    {
        return [
            'h1' => 'Seo H1',
            'title' => 'Seo Title',
            'keywords' => 'Seo Keywords',
            'description' => 'Seo Description',
        ];
    }

    public function isEmpty()
    {
        return (!$this->h1 && !$this->title && !$this->keywords && !$this->description);
    }
}