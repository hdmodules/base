<?php
namespace hdmodules\base\models;

use Yii;
use hdmodules\base\components\ActiveRecord;
use yii\validators\StringValidator;
use yii\behaviors\TimestampBehavior;


class Menu extends ActiveRecord {
    
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return "menu";
    }

    public function attributeLabels() {
        return [
            'name' => 'Name',
            'slug' => 'Slug'
        ];
    }

    public function rules() {
        return [
            [['name', 'slug'], 'required'],
            ['name', StringValidator::className()],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('base', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['slug', 'unique'],
        ];
    }

    public function behaviors() {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
            ]
        ];
    }

    /**
     * Return menu items by slug
     * @return mixed
     */
    public static function getMenuItemsBySlug($slug) {

        $items = MenuItem::find()->joinWith(['menu'])->where(['`menu`.slug' => $slug, 'status' => MenuItem::STATUS_ON])->orderBy('order_num asc')->all();
        
        return $items;
    }

    /**
     * Retrieve menu items
     * @return mixed
     */
    public function getMenuItems() {
        return $this->hasMany(MenuItem::className(), ['menu_id' => 'menu_id'])->orderBy('order_num asc');
    }

}
