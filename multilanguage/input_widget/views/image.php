<?php

use yii\helpers\Url;

$sid = uniqid()

?>

<ul class="nav nav-tabs" role="tablist">
    <?php foreach ($this->context->model->mlConfig['languages'] as $languageCode => $languageName): ?>

        <li class="<?= (Yii::$app->language == $languageCode) ? 'active' : '' ?>">
            <a href="#<?= $sid . $languageCode ?>" role="tab" data-toggle="tab">
                <?= $languageName ?>
            </a>
        </li>
    <?php endforeach ?>

</ul>

<div class="tab-content">

    <?php foreach ($this->context->model->mlConfig['languages'] as $languageCode => $languageName): ?>

        <?php
            $attribute = $this->context->attribute;

            if ( $languageCode != $this->context->model->mlConfig['default_language'] )
            {
                $attribute .= '_' . $languageCode;
            }
            $activeClass = (Yii::$app->language == $languageCode) ? 'active' : '';
        ?>

        <div class="tab-pane <?= $activeClass ?>" id="<?= $sid . $languageCode ?>">

            <?php $image = $this->context->model->$attribute; ?>

            <?php if (isset($image) && !empty($image)) : ?>

                <div style="margin: 10px 0">
                    <?php if(file_exists(Yii::getAlias('@frontend/web').$image)) : ?>

                        <img width=400px" src="<?= $image ?>" alt="">

                        <?php if ($this->context->model->hasProperty('mlConfig') AND isset($this->context->model->mlConfig['clear_image_url'])) {?>
                            <a href="<?= Url::to(['/admin/'.$this->context->model->mlConfig['clear_image_url'].'/clear-image', 'id' => $this->context->model->primaryKey, 'attr'=>$attribute]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('base', 'Clear image')?>" style="margin: 20px"><?= Yii::t('base', 'Clear image')?></a>
                        <?php } ?>

                    <?php endif; ?>
                </div>

                <div style="margin: 10px 0">
                    <?= file_exists(Yii::getAlias('@frontend/web').$image) ? '<a href="'. $this->context->model->$attribute .'" target="_blank">'. basename($this->context->model->$attribute).'</a>' . '  ('.   (filesize(Yii::getAlias('@frontend/web').$image)/1000000) . ' Mb)' : 'File not exist' ?>
                </div>

            <?php endif; ?>


            <?= $this->context->getInputField($attribute) ?>

        </div>


    <?php endforeach ?>
</div>