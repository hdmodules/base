<?php

namespace hdmodules\base\multilanguage\models;

use Yii;

/**
 * This is the model class for table "translations_with_string".
 *
 * @property integer $id
 * @property string $table_name
 * @property integer $model_id
 * @property string $attribute
 * @property string $lang
 * @property string $value
 */
class TranslationsWithString extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'translations_with_string';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id'], 'integer'],
            [['value'], 'string'],
            [['table_name', 'attribute', 'lang'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'table_name' => Yii::t('app', 'Table Name'),
            'model_id' => Yii::t('app', 'Model ID'),
            'attribute' => Yii::t('app', 'Attribute'),
            'lang' => Yii::t('app', 'Lang'),
            'value' => Yii::t('app', 'Value'),
        ];
    }
}
