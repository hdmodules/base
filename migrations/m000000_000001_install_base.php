<?php
use yii\db\Schema;

use \hdmodules\base\models\Photo;
use \hdmodules\base\models\SeoText;
use \hdmodules\base\models\Setting;
use \hdmodules\base\models\Publish;
use \hdmodules\base\models\Tag;
use \hdmodules\base\models\TagRelation;

class m000000_000001_install_base extends \yii\db\Migration
{
    public function SafeUp()
    {
        //PHOTO
        $this->createTable(Photo::tableName(), [
            'id' => Schema::TYPE_PK,
            'class' => Schema::TYPE_STRING . '(128) NOT NULL',
            'item_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'image' => Schema::TYPE_STRING . '(128) NOT NULL',
            'description' => Schema::TYPE_STRING . '(1024) NOT NULL',
            'order_num' => Schema::TYPE_INTEGER . " NOT NULL",
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createIndex('class', Photo::tableName(), ['class']);
        $this->createIndex('item_id', Photo::tableName(), ['item_id']);

        //SEOTEXT
        $this->createTable(SeoText::tableName(), [
            'id' => Schema::TYPE_PK,
            'class' => Schema::TYPE_STRING . '(128) NOT NULL',
            'item_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'h1' => Schema::TYPE_STRING . '(128) DEFAULT NULL',
            'title' => Schema::TYPE_STRING . '(128) DEFAULT NULL',
            'keywords' => Schema::TYPE_STRING . '(128) DEFAULT NULL',
            'description' => Schema::TYPE_STRING . '(128) DEFAULT NULL',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createIndex('class', SeoText::tableName(), ['class']);
        $this->createIndex('item_id', SeoText::tableName(), ['item_id']);

        //SETTINGS
        $this->createTable(Setting::tableName(), [
            'id' => 'pk',
            'name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'title' => Schema::TYPE_STRING . '(128) NOT NULL',
            'value' => Schema::TYPE_STRING . '(1024) NOT NULL',
            'visibility' => Schema::TYPE_BOOLEAN . " DEFAULT '0'",
        ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8');
        $this->createIndex('name', Setting::tableName(), 'name');

        //PUBLISH
        $this->createTable(Publish::tableName(), [
            'id' => 'pk',
            'model_id'=>Schema::TYPE_INTEGER,
            'table_name'=>Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'lang' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'status' => Schema::TYPE_BOOLEAN . " DEFAULT '0'"
        ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8');
        $this->createIndex('table_name', Publish::tableName(), 'table_name');
        $this->createIndex('model_id', Publish::tableName(), 'model_id');

        //TAGS
        $this->createTable(Tag::tableName(), [
            'id' => 'pk',
            'name'=>Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'frequency'=>Schema::TYPE_INTEGER,
        ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8');

        $this->createTable(TagRelation::tableName(), [
            'id' => 'pk',
            'table_name'=>Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'model_id'=>Schema::TYPE_INTEGER,
            'tag_id'=>Schema::TYPE_INTEGER,
        ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8');
        $this->createIndex('table_name', TagRelation::tableName(), 'table_name');
        $this->createIndex('model_id', TagRelation::tableName(), 'model_id');
        $this->createIndex('tag_id', TagRelation::tableName(), 'tag_id');

        $this->addForeignKey('fk1_tag_rel', 'tag_rel', 'tag_id', 'tag', 'id', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropTable(Photo::tableName());
        $this->dropTable(SeoText::tableName());
        $this->dropTable(Setting::tableName());
        $this->dropTable(Publish::tableName());
    }
}
