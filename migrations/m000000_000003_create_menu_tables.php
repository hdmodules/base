<?php

use yii\db\Schema;
use yii\db\Migration;

class m000000_000003_create_menu_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('menu', [
            'id' => $this->primaryKey(11),
            'name' => $this->string(128),
            'slug' => $this->string(128),
            'create_time'=> $this->integer()->notNull(),
            'update_time'=> $this->integer()
        ],  $tableOptions);

        $this->createTable('menu_item', [
            'id' => $this->primaryKey(11),
            'menu_id' => $this->integer()->notNull(),
            'parent_id' => $this->integer()->defaultValue(NULL),
            'label' => $this->string(128),
            'route' => $this->string(50),
            'params' => $this->string(255),
            'order_num' => $this->integer()->defaultValue(0),
            'status' => $this->integer(1)->defaultValue(0),
            'create_time'=> $this->integer()->notNull(),
            'update_time'=> $this->integer()
        ],  $tableOptions);

        $this->createIndex(
            'menu_id',
            'menu_item',
            'menu_id'
        );

        $this->addForeignKey(
            'fk-menu_id',
            'menu_item',
            'menu_id',
            'menu',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'parent_id',
            'menu_item',
            'parent_id'
        );

        $this->addForeignKey(
            'fk-parent_id',
            'menu_item',
            'parent_id',
            'menu_item',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('menu_item');
        $this->dropTable('menu');

        echo "m000000_000003_create_menu_tables reverted.\n";

        return true;
    }

}