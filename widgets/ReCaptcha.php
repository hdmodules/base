<?php

namespace hdmodules\base\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\widgets\InputWidget;
use hdmodules\base\models\Setting;

class ReCaptcha extends InputWidget {

    const JS_API_URL = 'https://www.google.com/recaptcha/api.js';
    const THEME_LIGHT = 'light';
    const THEME_DARK = 'dark';
    const TYPE_IMAGE = 'image';
    const TYPE_AUDIO = 'audio';

    /** @var string The color theme of the widget. [[THEME_LIGHT]] (default) or [[THEME_DARK]] */
    public $theme;

    /** @var string The type of CAPTCHA to serve. [[TYPE_IMAGE]] (default) or [[TYPE_AUDIO]] */
    public $type;

    /** @var string Your JS callback function that's executed when the user submits a successful CAPTCHA response. */
    public $jsCallback;

    /** @var string Your JS callback function that's executed when the captcha is loaded. */
    public static $onLoadCallback;

    /** @var string Prepare vars for onload callback */
    private static $onLoadCallbackVariables;
    private $onLoadCallbackBegin = 'var onLoadCallback = function() {';
    private $onLoadCallbackEnd = '}';

    /** @var array Additional html widget options, such as `class`. */
    public $widgetOptions = [];

    /** @var int  */
    public static $cnt = 0;
    private $countId;
    private $containerId;

    public function init() {
        $this::$cnt++;

        $this->countId = $this::$cnt;
        $this->containerId = 'recaptcha-' . $this::$cnt;

        parent::init();
        if (!Setting::get('recaptcha_key')) {
            throw new InvalidConfigException('Required `recaptcha_key` setting isn\'t set.');
        }

        $view = $this->view;
        $view->registerJsFile(
                self::JS_API_URL . '?onload=onLoadCallback&render=explicit&hl=' . $this->getLanguageSuffix(), [
            'position' => $view::POS_END,
            'async' => true,
            'defer' => true
                ]
        );
    }

    public function run() {
        $this->customFieldPrepare();

        $divOptions = [
            'id' => 'recaptcha-' . $this::$cnt,
            'data-sitekey' => Setting::get('recaptcha_key'),
        ];

        $divOptions['data-callback'] = 'recaptchaCallback';

        if (empty($this->theme)) {
            $divOptions['data-theme'] = $this->theme;
        }
        if (empty($this->type)) {
            $divOptions['data-type'] = $this->type;
        }

        $divOptions = $divOptions + $this->widgetOptions;

        echo Html::tag('div', '', $divOptions);
    }

    /**
     * @return string
     */
    protected function getLanguageSuffix() {
        $currentAppLanguage = Yii::$app->language != 'sp' ? Yii::$app->language : 'es';
        $langsExceptions = ['zh_CN', 'zh_TW', 'zh_TW'];

        if (strpos($currentAppLanguage, '_') === false) {
            return $currentAppLanguage;
        }

        if (in_array($currentAppLanguage, $langsExceptions)) {
            return str_replace('_', '-', $currentAppLanguage);
        } else {
            return substr($currentAppLanguage, 0, strpos($currentAppLanguage, '_'));
        }
    }

    /**
     * Prepares input
     */
    protected function customFieldPrepare() {
        $view = $this->view;
        if ($this->hasModel()) {
            $inputName = Html::getInputName($this->model, $this->attribute);
            $inputId = Html::getInputId($this->model, $this->attribute);
        } else {
            $inputName = $this->name;
            $inputId = 'recaptcha-' . $this->name; // todo resolve
        }

        $jsCode = "var recaptchaCallback_" . $this->countId . " = function(response){var form = $('#" . $this->containerId . "').closest('form'), data = form.data('yiiActiveForm'); $.each(data.attributes, function() {this.status = 3;}); form.yiiActiveForm('validate');};";

        $this->jsCallback = 'recaptchaCallback';

        $this::$onLoadCallbackVariables .= "var recaptcha" . $this::$cnt . ";\n";

        $this::$onLoadCallback .= "
            recaptcha" . $this::$cnt . " = grecaptcha.render('" . $this->containerId . "', {
              'sitekey'  : '" . Setting::get('recaptcha_key') . "',
              'theme'    : '" . ($this->theme ? $this->theme : "light") . "',
              'callback' : recaptchaCallback_$this->countId
             });";

        $onLoadScript = $this::$onLoadCallbackVariables . $this->onLoadCallbackBegin . $this::$onLoadCallback . $this->onLoadCallbackEnd;

        $view->registerJs($jsCode, $view::POS_BEGIN);
        $view->registerJs($onLoadScript, $view::POS_HEAD, 'recaptcha_onload');
        echo Html::input('hidden', $inputName, null, ['id' => $inputId]);
    }

}
