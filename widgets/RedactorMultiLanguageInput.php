<?php
namespace hdmodules\base\widgets;

use hdmodules\base\components\ActiveRecord;
use yii\widgets\InputWidget;


class RedactorMultiLanguageInput extends InputWidget
{
    /**
     * Associated model
     * @var ActiveRecord
     */
    public $model;
    
    public $preset = 'full';
    /**
     * Name of the model's attribute that the widget should display textareas for
     * @var string
     */
    public $attribute;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('multi', ['preset' => $this->preset]);
        /*if ($this->model->hasProperty('mlConfig') AND count($this->model->mlConfig['languages']) > 1) {
            return $this->render('multi', ['preset' => $this->preset]);
        } else {
            return $this->render('single', ['preset' => $this->preset]);
        }*/
    }

    /**
     * Initiate the widget
     *
     * @param $model \yii\db\ActiveRecord Model object
     * @param $attribute string attribute
     * @param $config
     * @return string|void
     * @throws \Exception
     */
    public static function widget($model, $attribute, $config = [])
    {
        $config = array_merge_recursive(['model' => $model, 'attribute' => $attribute], $config);
        return parent::widget($config);
    }
}