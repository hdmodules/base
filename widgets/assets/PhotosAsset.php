<?php
namespace hdmodules\base\widgets\assets;

use yii\web\AssetBundle;

class PhotosAsset extends AssetBundle
{
    public $sourcePath = '@hdmodules/base/widgets/assets/photos';
    public $css = [
        'photos.css',
    ];
    public $js = [
        'photos.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
