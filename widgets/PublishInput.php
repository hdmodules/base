<?php
namespace hdmodules\base\widgets;

use yii\base\Widget;
use yii\base\InvalidConfigException;

class PublishInput extends Widget
{
    public $model;

    public function init()
    {
        parent::init();

        if (empty($this->model)) {
            throw new InvalidConfigException('Required `model` param isn\'t set.');
        }
    }

    public function run()
    {
        echo $this->render('publish_input', [
            'publishSet' => $this->model->publishSet
        ]);
    }

}