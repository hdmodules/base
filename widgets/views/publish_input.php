<?php
use yii\helpers\Html;

$labelOptions = ['class' => 'control-label'];
$inputOptions = ['class' => 'form-control'];
?>
<label class="control-label" style="margin-top: 20px">
   <?= Yii::t('base', 'Published')?>
</label>

<div>

    <?php foreach ($publishSet as $k=>$model){ ?>
        <?= Html::activeCheckbox($model, '['.$k.']status', ['labelOptions'=>['class'=>'checkbox-inline', 'style'=>'padding-right: 20px;margin-bottom:30px'], 'label'=>$model->lang]); ?>
    <?php } ?>

</div>