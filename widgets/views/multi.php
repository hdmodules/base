<?php

use hdmodules\base\widgets\Redactor;
use yii\helpers\Url;
use yii\web\View;

?>
<?php $sid = uniqid() ?>

<?= \yii\helpers\Html::activeLabel($this->context->model, $this->context->attribute); ?>

<ul class="nav nav-tabs" role="tablist">
    <?php foreach ($this->context->model->mlConfig['languages'] as $languageCode => $languageName): ?>

        <li class="<?= (Yii::$app->language == $languageCode) ? 'active' : '' ?>">
            <a href="#<?= $sid . $languageCode ?>" role="tab" data-toggle="tab">
                <?= $languageName ?>
            </a>
        </li>
    <?php endforeach ?>

</ul>

<div class="tab-content">

    <?php foreach ($this->context->model->mlConfig['languages'] as $languageCode => $languageName): ?>

        <?php
            $attribute = $this->context->attribute;
    
            if ( $languageCode != $this->context->model->mlConfig['default_language'] )
            {
                $attribute .= '_' . $languageCode;
            }
    
            $activeClass = (Yii::$app->language == $languageCode) ? 'active' : '';
        ?>


        <div class="tab-pane <?= $activeClass ?>" id="<?= $sid . $languageCode ?>">

            <?php
            $this->context->options['id'] = $attribute; // overrides user input

            $options = [
                'id' => $attribute,
                'clientOptions' => [
                   'allowedContent' => true,
                   /* 'toolbar' => [
                        [
                            'name' => 'row0',
                            'items' => [
                                'Sourcedialog'
                            ]
                        ],
                        [
                            'name' => 'row1',
                            'items' => [
                                'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo', 'Redo'
                            ]
                        ],
                        [
                            'name' => 'row2',
                            'items' => [
                                'Find', 'Replace', 'SelectAll'
                            ]
                        ],
                        [
                            'name' => 'row3',
                            'items' => [
                                "Bold", "Italic", "Underline", "Strike", "Subscript", "Superscript", "RemoveFormat"
                            ]
                        ],
                        [
                            'name' => 'row7',
                            'items' => [
                                "Link", "Unlink", "Anchor"
                            ]
                        ],
                        [
                            'name' => 'row4',
                            'items' => [
                                "NumberedList", "BulletedList", "Blockquote",  "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "-"
                            ]
                        ],
                        [
                            'name' => 'row5',
                            'items' => [
                                "Image", "Table", "HorizontalRule"
                            ]
                        ],
                        [
                            'name' => 'row6',
                            'items' => [
                                "Styles", "Format", "Font", "FontSize", "TextColor", "UIColor"
                            ]
                        ]
                    ],*/
                    'extraPlugins' => 'uicolor,imageuploader,customquote,lineheight'
                ]
            ];
            ?>

            <?= \dosamigos\ckeditor\CKEditor::widget([
                    'id' => $attribute,
                    'preset' => $preset,
                    'model' => $this->context->model,
                    'attribute' => $attribute,
                    'options' => $options
                ]);
                ?>
        </div>


    <?php endforeach ?>
</div>