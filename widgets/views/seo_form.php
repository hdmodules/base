<?php
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;
use hdmodules\base\multilanguage\input_widget\MultiLanguageActiveField;

BootstrapPluginAsset::register($this);

$labelOptions = ['class' => 'control-label'];
$inputOptions = ['class' => 'form-control'];
?>
<p>
    <a class="dashed-link collapsed" data-toggle="collapse" href="#seo-form" aria-expanded="false" aria-controls="seo-form"><?= Yii::t('base', 'Seo texts')?></a>
</p>

<div class="collapse" id="seo-form">
    <div class="form-group">
        <?= Html::activeLabel($model, 'h1', $labelOptions) ?>
        <?= MultiLanguageActiveField::widget(['model'=>$model, 'attribute'=>'h1']) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'title', $labelOptions) ?>
        <?= MultiLanguageActiveField::widget(['model'=>$model, 'attribute'=>'title']) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'keywords', $labelOptions) ?>
        <?= MultiLanguageActiveField::widget(['model'=>$model, 'attribute'=>'keywords']) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'description', $labelOptions) ?>
        <?= MultiLanguageActiveField::widget(['model'=>$model, 'attribute'=>'description']) ?>
    </div>
</div>